import psutil
import argparse
import subprocess
import time
import csv


NoS = psutil.cpu_count()

parser = argparse.ArgumentParser(description='Program to start the process and collect statistics.')
parser.add_argument('exePath', type=str, help='Path to the executable file')
parser.add_argument('-s', type=int, default=10, help='The frequency of data collection in seconds (default: 10)')
parser.add_argument('-o', type=str, default='output.csv', help='Output file for statistics (default: output.csv)')
args = parser.parse_args()

def output_data(data: list, path=args.o) -> None:
    with open(path, "a", newline='') as csv_file:
        writer = csv.writer(csv_file, delimiter=';')
        writer.writerow(data)

proc = subprocess.Popen(args.exePath, stdout = subprocess.DEVNULL, stderr = subprocess.DEVNULL)
proc = psutil.Process(pid=proc.pid)

while True:
    data_list = [proc.cpu_percent()/NoS, proc.memory_full_info()[0], proc.memory_full_info()[1], proc.num_fds()]
    output_data(data_list)
    print(f'cpu: {data_list[0]}%')
    print(f'Resident Set Size: {data_list[1]}')
    print(f'Virtual Memory Size: {data_list[2]}')
    print(f'Descriptors: {data_list[3]}')
    time.sleep(args.s)

#python3 zapuskator_linux.py /usr/bin/firefox
#python3 zapuskator_linux.py /usr/bin/firefox -s 15 -o outputfile.csv