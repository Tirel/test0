usage: zapuskator_linux.py [-h] [-s S] [-o O] exePath

Program to start the process and collect statistics.

positional arguments:
  exePath     Path to the executable file

options:
  -h, --help  show this help message and exit
  -s S        The frequency of data collection in seconds (default: 10)
  -o O        Output file for statistics (default: output.csv)

Examples:
  python3 zapuskator_linux.py /usr/bin/firefox
  python3 zapuskator_linux.py /usr/bin/firefox -s 15 -o outputfile.csv
